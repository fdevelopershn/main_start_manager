<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Role_User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

class UserController extends Controller
{
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowUsers(Request $request)
    {
        if (Auth::user()->id_role!=3) {
          $users = User::Name($request->get('name'))
          ->select('users.id','users.name','users.email','users.last_login'
          ,'role_users.role','users.id_role','users.image')
          ->join('role_users','role_users.id','=','users.id_role')
          ->orderby('users.id','DESC')
          ->Paginate(3);

          return view('manager.users.users',compact('users'));
        }else {

          return redirect('/admin/main_start')
        ->with('danger','You need administrator privileges');    
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateUser()
    {
      if (Auth::user()->id_role!=3) {
        $role_users = Role_User::all();

        return view('manager.users.create_user',compact('role_users'));
      }else{

        return redirect('/admin/main_start')
       ->with('danger','You need administrator privileges');
      }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreUser(Request $request)
    {
      if (Auth::user()->id_role!=3) {
      $user = new User();

      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->id_role = $request->input('role_user');

      if ($request->file('image')=="") {
        $user->image = "default.png";
      }else {
        $file_user_image = $request->file('image');
        $destinationPath= 'img/users';
        $image_user_extension = Input::file('image')->getClientOriginalExtension();
        $image = uniqid()."-".$user->name.".".$image_user_extension;
        $user->image = $image;
        $file_user_image->move($destinationPath, $image);
      }

      /* Validation of fields */
      $this->validate($request,[
        'name' => 'required',
        'email' => 'required|email',
        'role_user' => 'required',
        'password' => 'required|min:8',
        'confirm_password' => 'required|min:8',
      ]);

      if($request->input('password')===$request->input('confirm_password')){
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect(action('UserController@ShowUsers'))
        ->with('success','The User "'.  $user->name . '" has been created');
      }else{
        return redirect(action('UserController@CreateUser'))
        ->with('danger','Passwords do not match');
      }

    }else{

      return  redirect('/admin/main_start')
     ->with('danger','You need administrator privileges');
    }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditUser($id)
    {
        $user = User::find($id);
        if ($user=="") {
          $user = User::find(Auth::user()->id);
          $role_users = Role_User::all();
          $role = Role_User::where('id',$user->id_role)->first();
        
          return view('manager.users.edit_user',compact('user','role_users','role'));
        }

        if(Auth::user()->id_role==1 || (Auth::user()->id_role==2 && $user->id_role==3))
        {
          $role_users = Role_User::all();
          $role = Role_User::where('id',$user->id_role)->first();
          return view('manager.users.edit_user',compact('user','role_users','role'));
        }
        
        if(Auth::user()->id_role==3 || (Auth::user()->id_role==2 && $user->id_role<3))
        {
          $user = User::find(Auth::user()->id);
          $role_users = Role_User::all();
          $role = Role_User::where('id',$user->id_role)->first();
         
          return view('manager.users.edit_user',compact('user','role_users','role'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateUser(Request $request, $id)
    {
      $user = User::find($id);
      $user->name = $request->input('name');
      $user->email = $request->input('email');
      $user->id_role = $request->input('role_user');

      if ($request->file('image')=="") {
        
      }else {
        $file_user_image = $request->file('image');
        $destinationPath= 'img/users';
        $image_user_extension = Input::file('image')->getClientOriginalExtension();
        $image = uniqid()."-".$user->name.".".$image_user_extension;
        $user->image = $image;
        $file_user_image->move($destinationPath, $image);
      }

      /* Validation of fields */
      $this->validate($request,[
        'name' => 'required',
        'email' => 'required|email',
        'role_user' => 'required',
      ]);
      
      if($request->input('password')!=""){

        /* Validation of Password fields */
        $this->validate($request,[
          'password' => 'min:8',
          'confirm_password' => 'min:8',
        ]);

        if($request->input('password')===$request->input('confirm_password')){
          $user->password = bcrypt($request->input('password'));
          $user->save();
          if (Auth::user()->id_role!=3) {
          return redirect(action('UserController@ShowUsers'))
          ->with('success','The User "'.  $user->name . '" has been edited');
          }else{
            return redirect('/admin/main_start')
           ->with('success','The User "'.  $user->name . '" has been edited');
          }

        }else{
          return redirect()->back()->with('danger','Passwords do not match');
        }

      }else{
        $user->save();
        if (Auth::user()->id_role!=3) {
          return redirect(action('UserController@ShowUsers'))
          ->with('success','The User "'.  $user->name . '" has been edited');
          }else{
            return redirect('/admin/main_start')
           ->with('success','The User "'.  $user->name . '" has been edited');
          }
      }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyUser($id)
    {
      if (Auth::user()->id_role!=3) {
        $user = User::find($id);
        $user->delete();
        if ($user->image=="default.png") {
          # code...
        }else{
          File::Delete('img/users/'.$user->image);  
        }
        return redirect(action('UserController@ShowUsers'))
        ->with('danger','The User "'.  $user->name . '" has been deleted');
        }else{
          return redirect('/admin/main_start')
          ->with('danger','You need administrator privileges');
        }
        
    }
}
