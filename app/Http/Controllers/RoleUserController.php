<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role_User;
use Illuminate\Support\Facades\Auth;

class RoleUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ShowRoleUsers()
    {
      if (Auth::user()->id_role==1) {
      $role_users = Role_User::all();

      return view('manager.role_users.role_users',compact('role_users'));
      }else{

      return redirect(action('UserController@ShowLoginDate'))
      ->with('danger','You need administrator privileges');
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function CreateRoleUser()
    {
      if (Auth::user()->id_role==1) {
        return view('manager.role_users.create_role_users');
      }else{

        return redirect(action('UserController@ShowLoginDate'))
        ->with('danger','You need administrator privileges');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function StoreRoleUser(Request $request)
    {
      if (Auth::user()->id_role==1) {
      $role_user = new Role_User();
      $role_user->role = $request->input('role');

      /* Validation of fields */
      $this->validate($request,[
        'role' => 'required',
      ]);

      $role_user->save();
      return redirect(action('RoleUserController@ShowRoleUsers'))
      ->with('success','The Role User "'.  $role_user->role . '" has been created');
      }else{

        return redirect(action('UserController@ShowLoginDate'))
        ->with('danger','You need administrator privileges');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function EditRoleUser($id)
    {
      if (Auth::user()->id_role==1) {
      $role_user = Role_User::find($id);
      return view('manager.role_users.edit_role_users',compact('role_user'));
      }else{

        return redirect(action('UserController@ShowLoginDate'))
        ->with('danger','You need administrator privileges');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function UpdateRoleUser(Request $request, $id)
    {
      if (Auth::user()->id_role==1) {
      $role_user = Role_User::find($id);
      $role_user->role = $request->input('role');

      /* Validation of fields */
      $this->validate($request,[
        'role' => 'required',
      ]);

      $role_user->save();

      return redirect(action('RoleUserController@ShowRoleUsers'))
      ->with('success','The Role User "'.  $role_user->role . '" has been edited');
      }else{

        return redirect(action('UserController@ShowLoginDate'))
        ->with('danger','You need administrator privileges');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function DestroyRoleUser($id)
    {
      if (Auth::user()->id_role==1) {
        $role_user = Role_User::find($id);
        $role_user->delete();

        return redirect(action('RoleUserController@ShowRoleUsers'))
        ->with('danger','The Role User "'.  $role_user->role . '" has been deleted');
      }else{

        return redirect(action('UserController@ShowLoginDate'))
        ->with('danger','You need administrator privileges');
        }
    }
}
