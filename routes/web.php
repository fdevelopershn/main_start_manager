<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('/admin/login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/', function () {
    return view('manager.main_login');
});

/* Autenticacion */
Route::group(['middleware' => ['auth']], function() {

Route::view('/admin/main_start','manager.main_start');

/* User routes */
Route::get('/users', 'UserController@ShowUsers');
Route::get('/users/create', 'UserController@CreateUser');
Route::post('/users/store', 'UserController@StoreUser');
Route::get('/users/edit/{id}', 'UserController@EditUser');
Route::post('/users/update/{id}', 'UserController@UpdateUser');
Route::get('/users/destroy/{id}', 'UserController@DestroyUser');

/* Role users */
Route::get('/role_users', 'RoleUserController@ShowRoleUsers');
Route::get('/role_users/create', 'RoleUserController@CreateRoleUser');
Route::post('/role_users/store', 'RoleUserController@StoreRoleUser');
Route::get('/role_users/edit/{id}', 'RoleUserController@EditRoleUser');
Route::post('/role_users/update/{id}', 'RoleUserController@UpdateRoleUser');
Route::get('/role_users/destroy/{id}', 'RoleUserController@DestroyRoleUser');

});
