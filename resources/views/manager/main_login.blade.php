@extends('layouts.principal')

@section('title', 'AdminPanel')

@section('content')
<nav class="navbar navbar-expand-lg navbar-dark fixed-top bg-dark flex-md-nowrap p-0">
   <a class="logo navbar-brand col-sm-3 col-md-2 mr-0" href="#">Company name</a>
   <form class="form-inline">
    <input class="form-control form-control-dark mr-sm-2" type="search" placeholder="Search" aria-label="Search">
    <button class="btn btn-outline-info my-2 my-sm-0" type="submit">Search</button>
  </form>
   <ul class="navbar-nav px-3">
     <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Gerson Alvarez</a>
      <div class="dropdown-menu" aria-labelledby="dropdown01">
        <a class="dropdown-item" href="#">Edit Profile</a>
        <a class="dropdown-item" href="#">Logout</a>
      </div>
    </li>
   </ul>
</nav>
<div class="container-fluid">
  <div class="row">
    <nav class="col-md-2 d-none d-md-block bg-light sidebar">
      <div class="sidebar-sticky">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" href="#">
              <span data-feather="home"></span>
              Dashboard <span class="sr-only">(current)</span>
            </a>
          </li>
          <div class="accordion" id="accordionExample">
            <li class="nav-item">
              <a class="nav-link" href="">
                <span data-feather="file"></span>
                Admin Users
              </a>
            </li>
          </div>
        </ul>
      </div>
    </nav>
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Dashboard</h1>
      </div>
    </main>
  </div>
</div>
@endsection
