@extends('layouts.app')

@section('content')
<a href="/role_users/create" type="button" class="btn btn-success">Add User Role</a>

{{-- Success message --}}
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

{{-- Danger message --}}
@if ($message = Session::get('danger'))
<div class="alert alert-danger">
    <p>{{ $message }}</p>
</div>
@endif

<table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">User Role</th>
            @if (Auth::user()->id_role===1)
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
            @endif
          </tr>
        </thead>
        <tbody>
            @foreach ($role_users as $key => $role)
                <tr>
                 <th scope="row">{{ $key+1 }}</th>
                 <td>{{ $role->role }}</td>
                 @if (Auth::user()->id_role===1)
                 <td><a type="button" href="/role_users/edit/{{ $role->id }}"
                  class="btn btn-primary">Edit</a></td>
                 <td><a type="button" href="/role_users/destroy/{{ $role->id }}"
                  onClick="return confirm('¿Are you sure you want to delete this user role?');"
                  class="btn btn-danger" >Delete</a></td>
                  @endif
               </tr>
            @endforeach
        </tbody>
      </table>
      <a href="/" type="button" class="btn btn-primary">Atras</a>
@endsection
