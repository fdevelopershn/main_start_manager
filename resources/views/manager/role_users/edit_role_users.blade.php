@extends('layouts.app')

@section('content')
@if (Auth::user()->id_role === 4)
    <form method="post" action="{{ URL('/role_users/update/'.$role_user->id) }}" >
      @csrf

      {{-- Validation alert --}}
      @if ($errors->all())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

        <div class="form-group">
          <label>Role</label>
          <input type="text" class="form-control" name="role" value="{{$role_user->role}}" id="type_account" placeholder="Enter a Role">
        </div>

        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/role_users" type="button" class="btn btn-primary">Atras</a>
      </form>
      @else
      <div class="container-fluid h-100">
        <div class="row w-100 align-items-center">
          <div class="col text-center">
            <div class="title m-b-md">
              This task requires Super Administrator privileges
            </div>
            <a href="/role_users" type="button" class="btn btn-primary">Atras</a>
          </div>
        </div>
      </div>
      @endif
@endsection
