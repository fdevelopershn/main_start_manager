@extends('layouts.app')

@section('content')
 <div class="flex-center position-ref">

    <div class="content">
        @if (Route::has('login'))
            <div class="title m-b-md">
              <h1>Admin Panel</h1>
              <h2>Freelance Developers</h2>
            </div>

            @auth
            <div class="title m-b-md">
                 Bienvenido {{ Auth::user()->name }}
            </div>

             {{-- Alert Administrator Privileges--}}
            @if ($message = Session::get('danger'))
            <div class="alert alert-danger">
                <h4>{{ $message }}</h4>
            </div>
            @endif

             {{-- Alert Edit Profil User--}}
             @if ($message = Session::get('success'))
             <div class="alert alert-success">
                 <h4>{{ $message }}</h4>
             </div>
             @endif
            @endauth

            <div class="links">
                @auth
                    @switch(Auth::user()->id_role)
                        @case(1)
                        <a href="{{ url('/users') }}">Users</a>
                        <a href="{{ url('/role_users') }}">Role Users</a>
                            @break
                        @case(2)
                        <a href="{{ url('/users') }}">Users</a>
                            @break
                    @endswitch

                    <a href="{{ url('/users/edit/'.Auth::user()->id) }}">Edit Profile</a>
                    <a href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                    </a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>

                @else
                    <a href="{{ url('/admin/login') }}">Login</a>
                @endauth
            </div>
         @endif

    </div>
</div>
@endsection
