@extends('layouts.app')

@section('content')

    <form method="post" action="{{  URL('/users/update/'.$user->id) }}" enctype="multipart/form-data">
      @csrf

      {{-- Validation alert --}}
      @if ($message = Session::get('danger'))
      <div class="alert alert-danger">
        <li>{{ $message }}</li>
      </div>
      @endif

      {{-- Validation alert --}}
      @if ($errors->all())
      <div class="alert alert-danger">
        <ul>
          @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <label>Add Image of User (Optional)</label>
        
        <div class="form-group">
          <div id="container-logo"  >
              <img class="img-responsive" width="150" style="border-radius:50px" id="logo-img" src="/img/users/{{ $user->image}}" />
          </div>
        </div>
        
        <div class="form-group">
        <label class="btn btn-primary">
            Search Image<input style="display:none" name="image" type="file">
        </label>
        <span class="help-block"></span>
        </div>

        <div class="form-group">
          <label>Name User</label>
          <input type="text" class="form-control" name="name" value="{{ $user->name }}">
        </div>

        <div class="form-group">
          <label>Email User</label>
          <input type="email" class="form-control" name="email" value="{{ $user->email }}">
        </div>

        @if (Auth::user()->id_role!=3)
        <div class="form-group">
          <label for="Type">Select Role of User </label>
          <select class="form-control" name="role_user">
            <option value="{{ $role->id }}">{{$role->role}}</option>
            @foreach ($role_users as $key => $role_user)
              @if (Auth::user()->id_role==1)
                @if ($role->id!=$role_user->id )
                <option value="{{ $role_user->id }}">{{$role_user->role}}</option>
                @endif
              @else
                @if ($role->id!=$role_user->id)
                  @if ($role_user->id>=Auth::user()->id_role)
                  <option value="{{ $role_user->id }}">{{$role_user->role}}</option>
                  @endif
                @endif
              @endif
            @endforeach
          </select>
        </div>
        @endif

        <div class="form-group">
          <label>Change Password User</label>
          <input type="password" class="form-control" name="password" placeholder="Enter Password of User">
        </div>

        <div class="form-group">
            <label>Confirm Password</label>
            <input type="password" class="form-control" name="confirm_password" placeholder="Enter Confirm of Password of User">
          </div>

        <button type="submit" class="btn btn-success">Submit</button>
        <a href="/users" type="button" class="btn btn-primary">Atras</a>
    </form>
@endsection
