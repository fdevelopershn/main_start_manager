@extends('layouts.app')

@section('content')

<form  action="{{ URL('/users') }}" method="get" role="form">
  <div class="form-group row">
      <div class="col-sm-3">
          <a href="/users/create" type="button" class="btn btn-success">Add User</a>
        </div>
        <div class="col-sm-1">
            <a type="button" class="btn btn-outline-secondary" href="/users">Refresh</a>
      </div>
     
 
    <div class="col-sm-8">
        <div class="input-group mb-8">
        <input name="name" type="text" class="form-control" placeholder="Search User" >
        <div class="input-group-append">
            <button class="btn btn-primary" type="submit">Search</button>
        </div>
      </div>
      </div>
     
</div>

</form>
{{-- Success message --}}
@if ($message = Session::get('success'))
<div class="alert alert-success">
    <p>{{ $message }}</p>
</div>
@endif

{{-- Danger message --}}
@if ($message = Session::get('danger'))
<div class="alert alert-danger">
    <p>{{ $message }}</p>
</div>
@endif

<div class="table-responsive">
<table class="table">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name User</th>
            <th scope="col">Email User</th>
            <th scope="col">Role User</th>
            <th scope="col">Image User</th>
            <th scope="col">Edit</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>
            @foreach ($users as $key => $user)
              @if (Auth::user()->id_role==1)
              <tr>
                <th scope="row">{{ $key+1 }}</th>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->role }}</td>    
                <td> <img style="width:50px;border-radius:30px"
                  src="/img/users/{{$user->image}}" alt="Ninguna"></td>      
                <td><a type="button" href="/users/edit/{{ $user->id }}"
                  class="btn btn-primary">Edit</a></td>           
                <td><a type="button" href="/users/destroy/{{ $user->id }}"
                  onClick="return confirm('¿Are you sure you want to delete this user ?');"
                  class="btn btn-danger">Delete</a></td>
              </tr>
              @endif
              @if (Auth::user()->id_role==2)
                @if ($user->id_role>2)
                <tr>
                    <th scope="row">{{ $key+1 }}</th>
                    <td>{{ $user->name }}</td>
                    <td>{{ $user->email }}</td>
                    <td>{{ $user->role }}</td>         
                    <td><a type="button" href="/users/edit/{{ $user->id }}"
                      class="btn btn-primary">Edit</a></td>           
                    <td><a type="button" href="/users/destroy/{{ $user->id }}"
                      onClick="return confirm('¿Are you sure you want to delete this user ?');"
                      class="btn btn-danger">Delete</a></td>
                  </tr>
                @endif
              @endif
            @endforeach
            
        </tbody>
</table>
</div>
{{ $users->links() }}
@if (Auth::user()->id_role!=3)
<table class="table">
        <h4>List User Login</h4>
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name User</th>
        <th scope="col">Date User</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($users as $key => $user)
            <tr>
             <th scope="row">{{ $key+1 }}</th>
             <td>{{ $user->name }}</td>
             <td>{{  strftime("%d %B del %Y a las %I:%M:%S %p", strtotime($user->last_login)) }}</td>
           </tr>
        @endforeach
    </tbody>
</table>
@endif
      <a href="/" type="button" class="btn btn-primary">Atras</a>
@endsection
