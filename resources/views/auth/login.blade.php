@extends('layouts.principal')

@section('title', 'Signin')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">

@section('content')
<form class="form-signin" method="POST" action="{{ route('login') }}">
  <img class="mb-4 size-icon" src="img/icon-freelance.png" alt="icon Freelance Developers">
  <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
   @csrf

   <div class="form-group">
       <label for="email" class="sr-only">{{ __('E-Mail Address') }}</label>
       <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email address" required autofocus>
           @if ($errors->has('email'))
               <span class="invalid-feedback" role="alert">
                   <strong>{{ $errors->first('email') }}</strong>
               </span>
           @endif
   </div>

   <div class="form-group">
       <label for="password" class="sr-only">{{ __('Password') }}</label>
       <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Password" required>

       @if ($errors->has('password'))
           <span class="invalid-feedback" role="alert">
               <strong>{{ $errors->first('password') }}</strong>
           </span>
       @endif
   </div>
   	<div class="checkbox mb-3">
      <label>
        <input type="checkbox" value="remember-me"> Remember me
      </label>
    </div>
    <div class="form-group">
      <button type="submit" class="btn btn-lg btn-primary btn-block">
          {{ __('Login') }}
      </button>

      @if (Route::has('password.request'))
          <a class="btn btn-link" href="{{ route('password.request') }}">
              {{ __('Forgot Your Password?') }}
          </a>
      @endif
    </div>
    <p class="mt-5 mb-3 text-muted">&copy; 2019 Freelance Developers</p>
</form>
@endsection
