<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>FreelanceDP | @yield('title')</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  </head>
  <body>
    @yield('content')
    <script src="{{ asset('js/app.js') }}" defer></script>
  </body>
</html>
